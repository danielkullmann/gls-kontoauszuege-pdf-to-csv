#!/usr/bin/python3

"""
Imports all PDF files that seem to contain GLS Kontoauszüge (account balances),
extracts the information, and saves the data as csv files.

The script expects one parameter, which is the directory to search through for
the pdf files.
"""
import csv
from collections import defaultdict
import json
import os
import re
import sys

from pdfminer.converter import PDFPageAggregator
from pdfminer.layout import LAParams, LTTextBoxHorizontal, LTFigure, LTChar, LTImage
from pdfminer.pdfdocument import PDFDocument
from pdfminer.pdfinterp import PDFPageInterpreter, PDFResourceManager
from pdfminer.pdfpage import PDFPage
from pdfminer.pdfparser import PDFParser

DATE_RE = re.compile("^([0-9]{1,2})\\.([0-9]{1,2})\\. (.*) ([0-9.]+,[0-9]+)([-+]| [SH])$")
FILENAME_RE1 = re.compile("([/\\\\]|^)AZG([0-9]+)_([0-9]+)_([0-9]+)\\.")
# 2033428600_2019_Nr.013_Kontoauszug_vom_31.12.2019_20201230110142.pdf
FILENAME_RE2 = re.compile("([/\\\\]|^)([0-9]+)_([0-9]+)_Nr\\.([0-9]+)_.*_([0-9.]+)_[0-9]*\\.")

def process_filename(filename):
    """ Returns (account_no, auszug_no, auszug_date, auszug_year) """
    match1 = FILENAME_RE1.search(filename)
    match2 = FILENAME_RE2.search(filename)
    def convert_date(date):
        return date[6:8] + "." + date[4:6] + "." + date[0:4]
    if match1:
        return match1.group(2), match1.group(3), convert_date(match1.group(4)), match1.group(4)[0:4]
    if match2:
        return match2.group(2), match2.group(4), match2.group(5), match2.group(3)

    return None, None, None

def process_page(interpreter, device, page):
    interpreter.process_page(page)
    layout = device.get_result()
    page = []
    for element in layout:
        if isinstance(element, LTTextBoxHorizontal):
            for obj in element:
                page.append(obj.get_text().rstrip())
        elif isinstance(element, LTFigure):
            last_y = -1
            line = []
            for obj in element:
                if isinstance(obj, LTChar):
                    y = obj.y0
                    if last_y != y:
                        if len(line) > 0:
                            page.append("".join(line))
                        line = []
                    line.append(obj.get_text())
                    last_y = y
                elif isinstance(obj, LTFigure) and isinstance(obj._objs[0], LTImage):
                    if len(line) > 0:
                        page.append("".join(line))
                    page.append("_______")
                    line = []
                else:
                    pass
            if len(line) > 0:
                page.append("".join(line))
    return page

def process_pdf(filename):
    fp = open(filename, "rb")
    parser = PDFParser(fp)
    laparams = LAParams()
    document = PDFDocument(parser)
    rsrcmgr = PDFResourceManager()
    device = PDFPageAggregator(rsrcmgr, laparams=laparams)
    interpreter = PDFPageInterpreter(rsrcmgr, device)
    pages = []
    for page in PDFPage.create_pages(document):
        pages.append(process_page(interpreter, device, page))
    return pages

def find_files(files, result = None):
    """ searches for GLS PDF files in the given directories"""
    if result is None:
        result = []
    for file in files:
        if os.path.isdir(file):
            files_in_dir = [os.path.join(file, item) for item in os.listdir(file)]
            find_files(files_in_dir, result)
        elif os.path.isfile(file) and file.endswith(".pdf") and ("AZG" in file or "Kontoauszug" in file):
            result.append(file)
    return result

def process_data(pages, account_number, auszug_number, auszug_date, auszug_year):
    items = []
    for page in pages:
        in_item = False
        item = {}
        for line in page:
            date_match = DATE_RE.match(line)
            if date_match:
                date = date_match.group(1) + "." + date_match.group(2) + "." + auszug_year
                in_item = True
                if item:
                    #print(item)
                    items.append(item)
                #print("s", line)
                value = float(date_match.group(4).replace(".", "").replace(",", "."))
                if date_match.group(5).strip() in ["-", "S"]:
                    value = -value
                item = {
                    "account": account_number,
                    "auszug_number": auszug_number,
                    "auszug_date": auszug_date,
                    "date": date,
                    "debit": date_match.group(5).strip() in ["-", "S"],
                    "value" : value,
                    "title": date_match.group(3).strip(),
                    "receiver": None,
                    "content": ""
                }
            elif in_item:
                #print("l", line)
                line = line.strip()
                if line == "" or line == "_" * len(line):
                    #print(item)
                    items.append(item)
                    item = {}
                    in_item = False
                else:
                    if item["debit"] is True and item["receiver"] is None and not line.startswith("Wertstellung:"):
                        item["receiver"] = line
                    elif line.startswith("Übertrag"):
                        # Skip lines that contain Übertrag information
                        pass
                    else:
                        item["content"] += line + "; "

        if item:
            #print(item)
            items.append(item)

    return items

def split_by_account_and_year(data):
    result = defaultdict(list)
    for item in data:
        account = item["account"]
        year = item["date"][6:10]
        key = account + "-" + year
        result[key].append(item)
    return result

def sort_key_function(item):
    date = item["date"]
    result = 0
    mult = 10000
    for part in reversed(date.split(".")):
        result = result * mult + int(part)
        mult = 100
    return result

def main():
    filenames = find_files(sys.argv[1:])

    items = []
    for filename in filenames:
        account_number, auszug_number, auszug_date, auszug_year = process_filename(filename)
        if account_number is None:
            print("WARNING:", filename, "is not recognized as GLS Kontoauszug")
            continue
        print("INFO: process", account_number, auszug_number, auszug_date, auszug_year, os.path.basename(filename))
        pages = process_pdf(filename)
        items.extend(process_data(pages, account_number, auszug_number, auszug_date, auszug_year))

    parts = split_by_account_and_year(items)

    for key, part in parts.items():
        with open(key+".csv", "w") as fh:
            writer = csv.DictWriter(fh, part[0].keys())
            writer.writeheader()
            # sort part by column "date"
            part = sorted(part, key=sort_key_function)
            for item in part:
                writer.writerow(item)


    #for item in items:
    #    print("")
    #    for k,v in item.items():
    #        print(k, v)

    #with open("result.json", "w") as fh:
    #    json.dump(items, fh, indent=2)

    #with open("result.csv", "w") as fh:
    #    writer = csv.DictWriter(fh, items[0].keys())
    #    writer.writeheader()
    #    for item in items:
    #        writer.writerow(item)

if __name__ == "__main__":
    main()
